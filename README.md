# AUTH API REST
***
![version](https://img.shields.io/badge/version-0.1.0-informational)

AUTH API con usuarios y roles. [Guía](https://bezkoder.com/node-js-jwt-authentication-postgresql/).

## Tabla de Contenido
***
1. [Tecnlogías](#tecnlogías)
2. [Configuración local](#configuración-local)
4. [Versionado](#versionado)
5. [Deploy](#deploy)

## Tecnlogías
***
- JavaScript
- Express
- PostgreSQL
- Sequelize
- JWT
- [Git](https://git-scm.com/) (con [GitFlow](https://www.atlassian.com/es/git/tutorials/comparing-workflows/gitflow-workflow#:~:text=El%20flujo%20de%20trabajo%20de,de%20la%20publicaci%C3%B3n%20del%20proyecto.))

## Configuración local
***
Clonarse el repo y ejecutar dentro de la carpeta del proyecto `npm install` para descargar todas las dependencias.

Cambiar las configuraciones de BD en `/app/config/db.config.js`.

Crear la BD.

## Versionado
***
Se utiliza el flujo de trabajo [GitFlow](https://www.atlassian.com/es/git/tutorials/comparing-workflows/gitflow-workflow#:~:text=El%20flujo%20de%20trabajo%20de,de%20la%20publicaci%C3%B3n%20del%20proyecto.):
- Rama de desarrollo: **development**
- Rama productiva: **master**
- Prefijo para features: **feature/**
- Prefijo para bugs: **bugfix/**
- Prefijo para hotfixes: **hotfix/**
- Prefijo para releases: **release/**

## Deploy
***
Para ejecutar la app, ejecutar el archivo `server.js` con:

`$ node server.js`.   