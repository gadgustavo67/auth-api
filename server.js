const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const PORT = process.env.PORT || 3000;

//Configuraciones
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Rutas
app.get("/", (req, res) => {
    res.status(200).send("<h1>¡Bienvenido AUTH API!</h1>");
});

//Archivos de Rutas
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);

//Levantar servidor en el puerto
app.listen(PORT, () => {
    console.log(`Servidor AUTH API ejecutándose en el puerto ${PORT}.`);
});

//Configuraciones de BD
const db = require("./app/models");
const Role = db.role;

db.sequelize.sync({ force: true }).then(() => {
    console.log('Eliminar y cargar datos en la BD.');
    initial();
});

function initial() {
    Role.create({
        id: 1,
        name: "superadmin"
    });
    Role.create({
        id: 2,
        name: "admin"
    });
    Role.create({
        id: 3,
        name: "moderador"
    });
    Role.create({
        id: 4,
        name: "usuario"
    });
}